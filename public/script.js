const supportedExtensions = ["jpeg", "jpg", "png", "svg", "gif", "webp"];

Dropzone.options.atlDropzone = {
  maxFilesize: 2,
  accept: function (file, done) {
    const arr = file.name.split(".");
    const fileExtension = arr[arr.length - 1];

    if (!supportedExtensions.includes(fileExtension)) {
      done("File extension is not supported...");
    } else {
      done();
    }
  },
};
