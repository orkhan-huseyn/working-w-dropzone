const express = require("express");
const fileUpload = require("express-fileupload");
const path = require("path");
const app = express();

app.use(express.static(path.join(__dirname, "public")));

app.use(
  fileUpload({
    useTempFiles: false,
    tempFileDir: path.join(__dirname, "tmp"),
  })
);

app.get("/", (req, res) => {
  res.sendFile(path.join(__dirname, "public", "index.html"));
});

app.get("/custom-dropzone", (req, res) => {
  res.sendFile(path.join(__dirname, "public", "index2.html"));
});

app.post("/upload", (req, res) => {
  if (!req.files || Object.keys(req.files).length === 0) {
    return res.status(400).send("No files were uploaded.");
  }

  let targetFile = req.files.file;

  targetFile.mv(path.join(__dirname, "uploads", targetFile.name), (err) => {
    if (err) return res.status(500).send(err);
    res.send("File uploaded!");
  });
});

app.listen(3000, () => console.log("Your app listening on port 3000"));
